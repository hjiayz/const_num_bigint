use core::str::FromStr;
use num_bigint::BigInt;
use num_bigint::BigUint;
use num_bigint::Sign;
use proc_macro::TokenStream;
use quote::quote;
use syn::Path;
use syn::__private::Span;
use syn::{parse_macro_input, Lit, LitInt};
use syn::{punctuated::Punctuated, token::Comma, Expr, ExprGroup, ExprLit, ExprPath, Ident};

#[proc_macro]
pub fn bigint_with_crate(input: TokenStream) -> TokenStream {
    let params: Punctuated<Expr, Comma> =
        parse_macro_input!(input with Punctuated::parse_terminated);
    make(params, false, false)
}

#[proc_macro]
pub fn biguint_with_crate(input: TokenStream) -> TokenStream {
    let params: Punctuated<Expr, Comma> =
        parse_macro_input!(input with Punctuated::parse_terminated);
    make(params, false, true)
}

#[proc_macro]
pub fn bigint_file_with_crate(input: TokenStream) -> TokenStream {
    let params: Punctuated<Expr, Comma> =
        parse_macro_input!(input with Punctuated::parse_terminated);
    make(params, true, false)
}

#[proc_macro]
pub fn biguint_file_with_crate(input: TokenStream) -> TokenStream {
    let params: Punctuated<Expr, Comma> =
        parse_macro_input!(input with Punctuated::parse_terminated);
    make(params, true, true)
}

fn make(params: Punctuated<Expr, Comma>, is_file: bool, is_uint: bool) -> TokenStream {
    let crate_name = match &params[0] {
        Expr::Path(ExprPath {
            path: Path { segments, .. },
            ..
        }) => segments[0].ident.to_owned(),
        _ => panic!("except crate name"),
    };
    let expr1 = match &params[1] {
        Expr::Group(ExprGroup { expr, .. }) => expr.to_owned(),
        other => panic!("except literal string {} ", quote! {#other}.to_string()),
    };
    let s = match expr1.as_ref() {
        Expr::Lit(ExprLit {
            lit: Lit::Str(s), ..
        }) => s,
        other => panic!("except literal string {} ", quote! {#other}.to_string()),
    };
    let s_value = if is_file {
        use std::env::var;
        use std::path::{Path, PathBuf};
        let dir = s.value();
        let mut dir = PathBuf::from(dir);
        if dir.is_relative() {
            dir = Path::new(&var("CARGO_MANIFEST_DIR").expect("CARGO_MANIFEST_DIR missing"))
                .join(dir);
        }
        String::from_utf8(
            std::fs::read(dir)
                .unwrap()
                .into_iter()
                .filter(|ch| ch.is_ascii_digit())
                .collect(),
        )
        .unwrap()
    } else {
        s.value()
    };
    if is_uint {
        let array32 = BigUint::from_str(&s_value).unwrap().to_u32_digits();
        uint_gen(crate_name, array32)
    } else {
        let (sign, array32) = BigInt::from_str(&s.value()).unwrap().to_u32_digits();
        int_gen(crate_name, sign, array32)
    }
    .into()
}

fn uint_gen(crate_name: Ident, array32: Vec<u32>) -> proc_macro2::TokenStream {
    let arr = quote_array32(array32);
    quote! {
        {
            const BIGUINT_CONST_VALUE : &'static BigUint = & #crate_name::ConstBigUint::new(&#crate_name::from_slice!(#arr)).to_biguint();
            BIGUINT_CONST_VALUE
        }
    }
}

fn int_gen(crate_name: Ident, sign: Sign, array32: Vec<u32>) -> proc_macro2::TokenStream {
    let arr = quote_array32(array32);
    let sign_lit = match sign {
        Sign::Minus => quote! {#crate_name::Sign::Minus},
        Sign::NoSign => quote! {#crate_name::Sign::NoSign},
        Sign::Plus => quote! {#crate_name::Sign::Plus},
    };
    quote! {
        {
            const BIGINT_CONST_VALUE : &'static BigInt = & #crate_name::ConstBigInt::new(#sign_lit,&#crate_name::from_slice!(#arr)).to_bigint();
            BIGINT_CONST_VALUE
        }
    }
}

fn quote_array32(array32: Vec<u32>) -> proc_macro2::TokenStream {
    let elems: Vec<Expr> = array32
        .into_iter()
        .map(|value32| {
            Expr::Lit(ExprLit {
                attrs: Default::default(),
                lit: Lit::Int(LitInt::new(&value32.to_string(), Span::call_site())),
            })
        })
        .collect();

    quote! {
        #(#elems),*
    }
}
