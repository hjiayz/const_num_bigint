//! const
//!
//! ```
//!     use const_num_bigint::*;
//!     use std::str::FromStr;
//!     const TEST_INT : &'static BigInt = bigint!("123");
//!     let bigint = BigInt::from_str("123").unwrap();
//!     assert!(&bigint==TEST_INT);
//!     const TEST_INT2 : &'static BigInt = bigint!("-123");
//!     let bigint2 = BigInt::from_str("-123").unwrap();
//!     assert!(&bigint2==TEST_INT2);
//!     const TEST_INT3 : &'static BigInt =
//!         bigint!("123123123123123123123123123123123123123123123123123123123123123123123123123123");
//!     let bigint3 = BigInt::from_str(
//!         "123123123123123123123123123123123123123123123123123123123123123123123123123123"
//!     ).unwrap();
//!     assert!(&bigint3==TEST_INT3);
//!     const TEST_UINT : &'static BigUint = biguint!(
//!         "123123123123123123123123123123123123123123123123123123123123123123123123123123"
//!     );
//!     let biguint = BigUint::from_str(
//!         "123123123123123123123123123123123123123123123123123123123123123123123123123123"
//!     ).unwrap();
//!     assert!(&biguint==TEST_UINT);
//!     assert!(&biguint==
//!         biguint!("123123123123123123123123123123123123123123123123123123123123123123123123123123")
//!     );
//!     let pi_str = include_str!("pi.num");
//!     let biguint_pi = BigUint::from_str(pi_str).unwrap();
//!     assert!(&biguint_pi==
//!         biguint_file!("src/pi.num")
//!     );
//! ```
//!
//!

pub use const_num_bigint_derive::*;
use const_std_vec::ConstVec;
pub use num_bigint::*;
use std::mem::transmute;

pub struct ConstBigInt {
    _sign: Sign,
    _data: ConstBigUint,
}

pub struct ConstBigUint {
    _data: ConstVec<usize>,
}

impl ConstBigUint {
    #[inline]
    pub const fn new(arr: &'static [usize]) -> Self {
        ConstBigUint {
            _data: ConstVec::new(arr),
        }
    }
    #[inline]
    pub const fn to_biguint(&'static self) -> &'static BigUint {
        unsafe { transmute(self) }
    }
}

impl ConstBigInt {
    #[inline]
    pub const fn new(sign: Sign, arr: &'static [usize]) -> Self {
        ConstBigInt {
            _sign: sign,
            _data: ConstBigUint::new(arr),
        }
    }
    #[inline]
    pub const fn to_bigint(&'static self) -> &'static BigInt {
        unsafe { transmute(self) }
    }
}

#[cfg(target_pointer_width = "64")]
#[macro_export]
macro_rules! from_slice {
    ($($lo:expr,$hi:expr),*) => {
        {
            [ $( ($lo) + (($hi)<<32) ),* ]
        }
    };
    ( $firstlo:expr $(,$hi:expr,$lo:expr)* ) => {
        {
            [ ($firstlo) $( + (($hi)<<32) , ($lo) )* ]
        }
    };
}

#[cfg(target_pointer_width = "32")]
#[macro_export]
macro_rules! from_slice {
    ( $firstlo:expr $(,$hi:expr,$lo:expr)* ) => {
        {
            [ ($firstlo) $( , ($hi) , ($lo) )* ]
        }
    };
    ($($lo:expr,$hi:expr),*) => {
        {
            [ $( ($lo) , ($hi) ),* ]
        }
    };
}

#[macro_export]
macro_rules! bigint {
    ( $num:expr ) => {
        $crate::bigint_with_crate!($crate, $num)
    };
}

#[macro_export]
macro_rules! bigint_file {
    ( $num:expr ) => {
        $crate::bigint_file_with_crate!($crate, $num)
    };
}

#[macro_export]
macro_rules! biguint {
    ( $num:expr ) => {
        $crate::biguint_with_crate!($crate, $num)
    };
}

#[macro_export]
macro_rules! biguint_file {
    ( $num:expr ) => {
        $crate::biguint_file_with_crate!($crate, $num)
    };
}
