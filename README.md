# const_num_bigint

const

```rust
    use const_num_bigint::*;
    use std::str::FromStr;
    const TEST_INT : &'static BigInt = bigint!("123");
    let bigint = BigInt::from_str("123").unwrap();
    assert!(&bigint==TEST_INT);
    const TEST_INT2 : &'static BigInt = bigint!("-123");
    let bigint2 = BigInt::from_str("-123").unwrap();
    assert!(&bigint2==TEST_INT2);
    const TEST_INT3 : &'static BigInt =
        bigint!("123123123123123123123123123123123123123123123123123123123123123123123123123123");
    let bigint3 = BigInt::from_str(
        "123123123123123123123123123123123123123123123123123123123123123123123123123123"
    ).unwrap();
    assert!(&bigint3==TEST_INT3);
    const TEST_UINT : &'static BigUint = biguint!(
        "123123123123123123123123123123123123123123123123123123123123123123123123123123"
    );
    let biguint = BigUint::from_str(
        "123123123123123123123123123123123123123123123123123123123123123123123123123123"
    ).unwrap();
    assert!(&biguint==TEST_UINT);
    assert!(&biguint==
        biguint!("123123123123123123123123123123123123123123123123123123123123123123123123123123")
    );
    let pi_str = include_str!("pi.num");
    let biguint_pi = BigUint::from_str(pi_str).unwrap();
    assert!(&biguint_pi==
        biguint_file!("src/pi.num")
    );
```



License: MIT OR Apache-2.0
